# Hazelcast Cluster

Authors: 
- *ivan.macalak@posam.sk*

### Info:

 This template creates 2 Hazelcast nodes in hazelcast-cluster stack that can be further extended. Additionally the Hazelcast management center node is configured as part of this stack. The template uses custom Docker Rancher aware image based on oficial Hazelcast Docker image.

### Usage:
 
 - Select Hazelcast Cluster from catalog.
 - Modify parameters according your needs.
 - You should have labeled host in your Rancher environment:
   - _hazelcast=master_  (for first Hazelcast node in the cluster)
   - _hazelcast=node_  (for all additional Hazelcast nodes)
   - _hazelcast=mc_  (for Hazelcast management center node)
 - Click deploy.
 - The Hazelcast nodes will be bound to the Rancher managed network.
 - You can further exytnd the existing Hazelcast cluster by increasing scale factor of the Hazelcast node service.
 
### Note:
The Hazelcast mng center requires license for more than two Hazelcast nodes in the cluster.