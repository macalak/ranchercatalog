# Couchbase Cluster

Authors: 
- *ivan.macalak@posam.sk*

### Info:

 This template creates 2 Couchbase nodes in couchbase-cluster stack that can be further extended. The minimal production deployment should contain 3 nodes.
 The template uses custom Docker Rancher aware image based on Couchbase community release Docker image. Admin console is reachable on default port 8081 via port mapping from the master node to host machine. 

### Usage:
 
 - Select Couchbase Cluster from catalog.
 - Modify parameters as admin user/password and memory sizes according your needs.
 - You should have labeled host in your Rancher environment:
   - _couchbase=master_  (for first Couchbase node in the cluster)
   - _couchbase=node_  (for all additional Couchbase nodes)
 - Click deploy.
 - The Couchbase nodes will be bound to the Rancher managed network.
 - You can further clone existing Couchbase node service in order to scale existing cluster.
 - You should not add any load balancer in front of Couchbase nodes as Couchbase provide load balancing internally based on internal data partitioning among nodes.
 
### Note:
 
 You can connect from DEV machine to master node only. You should record Couchbase host machine in the /etc/hosts  on DEV machine in order to speed up Client connection as Client uses the DNS Reverse lookup. 
 Services runnig in Rancher, which require Couchbase cluster connection should be configured accordingly, but with Rancher overlay network (10.42.0.0/16).