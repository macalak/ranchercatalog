# Couchbase Master Node

Authors: 
- *ivan.macalak@posam.sk*

### Info:

 This template creates single Couchbase master node. The template uses custom Docker Rancher aware image based on Couchbase community release Docker image. Admin console is reachable on default port 8081 via port mapping from the master node to host machine. 

### Usage:
 
 - Select Couchbase Master from catalog.
 - Modify parameters as admin user/password and memory sizes according your needs.
 - You should have labeled host in your Rancher environment:
   - _couchbase=master_ 
 - Click deploy.
 - The Couchbase will be bound to the Rancher managed network.
 
 